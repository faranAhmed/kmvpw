from ethereum import utils as u
from web3 import Web3, HTTPProvider, WebsocketProvider
from plasma.peer_wallets.contracts_abi.contracts_abis import RootChainAbi
from plasma.peer_wallets.deployer import Deployer

HTTPProviderRopsten = Web3(HTTPProvider('https://ropsten.infura.io/v3/0d7e3326444f486babd00801ccdadc28'))
HTTPProviderLocal = Web3(HTTPProvider('http://localhost:8545'))
WebsocketProviderRopsten = Web3(WebsocketProvider('wss://ropsten.infura.io/ws'))

RELAYER_ADDRESS = Web3.toChecksumAddress("0xc39b2048DB8c32677F8A7739DbD591363651552b")
PEER_WALLETS_ENGINE_ADDRESS = Web3.toChecksumAddress("0x51f3fc1842c22bdf1b285cc19da6b5dc723ebf59")
PEER_WALLETS_ADDRESS = Web3.toChecksumAddress("0x26373F165e47AfbcAf399e7c618c9A9c95DD0FDB")
ROOT_CHAIN_ADDRESS = Web3.toChecksumAddress("0x07F3fB05d8b7aF49450ee675A26A01592F922734")

AUTHORITY = {
    'address': '0x8F5498bAEC61169FDd532073693BB4F789727514',
    'key': u.normalize_key(b'e6270e7608743698981cfcdecc1e7343d36d2c7fcbe56f9700016b13c880d532')
}

ACCOUNTS = [
    {
        'address': Web3.toChecksumAddress(HTTPProviderLocal.eth.accounts[0]),
        'key': u.normalize_key(b'70f1384b24df3d2cdaca7974552ec28f055812ca5e4da7a0ccd0ac0f8a4a9b00')
    }
]

NULL_BYTE = b'\x00'
NULL_HASH = NULL_BYTE * 32
NULL_SIGNATURE = NULL_BYTE * 65
NULL_ADDRESS = NULL_BYTE * 20
NULL_ADDRESS_HEX = '0x' + NULL_ADDRESS.hex()

peer_wallets_engine_instance = Deployer().get_contract_at_address_env('PeerWalletsEngine', PEER_WALLETS_ENGINE_ADDRESS, HTTPProviderLocal, False)
peer_wallets_instance = Deployer().get_contract_at_address_env('PeerWallets', PEER_WALLETS_ADDRESS, HTTPProviderLocal, False)
root_chain_instance = Deployer().get_contract_instance(RootChainAbi, ROOT_CHAIN_ADDRESS, HTTPProviderRopsten, False)
relayer_instance = Deployer().get_contract_at_address_env('Relayer', RELAYER_ADDRESS, HTTPProviderLocal, False)
