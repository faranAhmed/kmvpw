from web3 import Web3
from plasma.peer_wallets.deployer import Deployer
from plasma_core.block import Block
from plasma_core.chain import Chain
from plasma_core.constants import HTTPProviderLocal, ACCOUNTS
from plasma_core.utils.transactions import get_deposit_tx, encode_utxo_id
from .root_event_listener import RootEventListener


class ChildChain(object):

    def __init__(self, operator, root_chain):
        self.operator = operator
        self.w3 = HTTPProviderLocal
        self.root_chain = root_chain
        self.chain = Chain(self.operator)
        self.current_block = Block(number=self.chain.next_child_block)

        # Listen for events
        self.event_listener = RootEventListener(root_chain, confirmations=0)
        self.event_listener.on('DepositEther', self.apply_deposit)
        self.event_listener.on('DepositKyber', self.send_tokens)

        self.event_listener.on('ExitStarted', self.apply_exit)

    def apply_exit(self, event):
        event_args = event['args']
        utxo_id = event_args['utxoPos']
        self.chain.mark_utxo_spent(utxo_id)

    def apply_launch(self, event):
        event_args = event['args']
        peer_wallets_contract_address = event_args['peerWallet']
        print(event_args)
        # launchInvestment to prevent from investment being triggered

        peer_wallets_instance = Deployer().get_contract_at_address_env('PeerWallets',
                                                                       peer_wallets_contract_address,
                                                                       self.w3,
                                                                       False)

        txn = peer_wallets_instance.functions.launchInvestment(int(0)). \
            buildTransaction({'from': ACCOUNTS[0]['address'],
                              'gas': 900000,
                              'gasPrice': 41000000000,
                              'nonce': self.w3.eth.getTransactionCount(ACCOUNTS[0]['address'])})

        raw_tx = {'nonce': txn['nonce'],
                  'gas': txn['gas'],
                  'gasPrice': txn['gasPrice'],
                  'to': txn['to'],
                  'data': txn['data'],
                  'from': txn['from']}

        signed_txn = self.w3.eth.account.signTransaction(dict(raw_tx), ACCOUNTS[0]['key'])
        self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        tx_hash = Web3.toHex(Web3.sha3(signed_txn.rawTransaction))
        self.w3.eth.waitForTransactionReceipt(tx_hash)
        print("Transaction For Launch Investment Successfully Submitted, Hash : ".format(tx_hash))

    def apply_deposit(self, event):
        event_args = event['args']
        owner = Web3.toChecksumAddress(event_args['depositor'])
        amount = event_args['amount']
        blknum = event_args['depositBlock']
        peer_wallets_contract_address = event_args['peerWallet']
        print("Event : 'DepositEther' triggered values")
        print(event_args)
        print()

        peer_wallets_instance = Deployer().get_contract_at_address_env('PeerWallets',
                                                                       peer_wallets_contract_address,
                                                                       self.w3,
                                                                       False)

        txn = peer_wallets_instance.functions.makeInvestment(amount, owner).transact()

        raw_tx = {'nonce': txn['nonce'],
                  'gas': txn['gas'],
                  'gasPrice': txn['gasPrice'],
                  'to': txn['to'],
                  'data': txn['data'],
                  'from': txn['from']}

        signed_txn = self.w3.eth.account.signTransaction(dict(raw_tx), ACCOUNTS[0]['key'])
        self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        tx_hash = Web3.toHex(Web3.sha3(signed_txn.rawTransaction))
        self.w3.eth.waitForTransactionReceipt(tx_hash)
        print("Transaction For Make Investment Successfully Submitted, Hash : {0}".format(tx_hash))

        deposit_tx = get_deposit_tx(owner, amount)
        deposit_block = Block([deposit_tx], number=blknum)
        self.chain.add_block(deposit_block)

    def send_tokens(self, event):
        event_args = event['args']
        peer_wallets_contract_address = Web3.toChecksumAddress(event_args['peerWallet'])
        exchange_group_key = event_args['destExchangeAddress']
        total_tokens = event_args['totalTokens']
        print("Event : 'DepositKyber' triggered values ")
        print(event_args)
        print()

        peer_wallets_instance = Deployer().get_contract_at_address_env('PeerWallets', peer_wallets_contract_address,
                                                                       self.w3,
                                                                       False)

        txn = peer_wallets_instance.functions.tradeWalletTokens(exchange_group_key, total_tokens).\
            buildTransaction({'from': ACCOUNTS[0]['address'],
                              'gas': Web3.toHex(900000),
                              'nonce': Web3.toHex(self.w3.eth.getTransactionCount(ACCOUNTS[0]['address'])),
                              'gasPrice': Web3.toHex(41000000000)})

        raw_tx = {'nonce': txn['nonce'],
                  'gas': txn['gas'],
                  'gasPrice': txn['gasPrice'],
                  'to': txn['to'],
                  'data': txn['data'],
                  'from': txn['from']}

        signed_txn = self.w3.eth.account.signTransaction(dict(raw_tx), ACCOUNTS[0]['key'])
        self.w3.eth.sendRawTransaction(signed_txn.rawTransaction)
        tx_hash = Web3.toHex(Web3.sha3(signed_txn.rawTransaction))
        self.w3.eth.waitForTransactionReceipt(tx_hash)
        print("Transaction Trade Wallet Tokens Successfully Submitted, Hash : {0}".format(tx_hash))

    def apply_transaction(self, tx):
        self.chain.validate_transaction(tx, self.current_block.spent_utxos)
        self.current_block.add_transaction(tx)
        return encode_utxo_id(self.current_block.number, len(self.current_block.transaction_set) - 1, 0)

    def submit_block(self, block):
        self.chain.add_block(block)
        self.root_chain.transact({
            'from': self.operator
        }).submitBlock(block.merkle.root)
        self.current_block = Block(number=self.chain.next_child_block)

    def get_transaction(self, tx_id):
        return self.chain.get_transaction(tx_id)

    def get_block(self, blknum):
        return self.chain.get_block(blknum)

    def get_current_block(self):
        return self.current_block
