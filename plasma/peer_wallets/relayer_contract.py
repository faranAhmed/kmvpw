from web3 import Web3
from .relayer_event_listener import RelayerListener
from plasma_core.constants import AUTHORITY, HTTPProviderLocal, HTTPProviderRopsten, peer_wallets_instance, \
    root_chain_instance


class Relayer(object):

    def __init__(self, operator, relayer):
        self.relayer = relayer
        self.operator = operator
        self.w3 = HTTPProviderRopsten
        self.w2 = HTTPProviderLocal

        # Listen for events
        self.event_listener = RelayerListener(self.relayer, confirmations=0)
        self.event_listener.on('LogTotal', self.apply_deposit_kyber_tokens)

    def apply_deposit_kyber_tokens(self, event):
        event_args = event['args']
        total_investment = event_args['totalInvested']
        peer_wallets_contract_address = event_args['peerWallet']
        print("Event for Total Investment triggered by Relayer Smart Contract")
        print(event_args)
        print()

        if total_investment > 0:
            max_keys = peer_wallets_instance.functions.getExchangeGroupsLength().call()

            for x in range(0, max_keys):
                exchange_group_keys = Web3.toChecksumAddress(peer_wallets_instance.functions.getExchangeGroupsKeyAt(x).call())
                distribution_at = peer_wallets_instance.functions.getDistributionAt(x).call()
                amount = distribution_at * total_investment
                amount = amount / 100

                txn = root_chain_instance.functions. \
                    depositKyberTokens(Web3.toChecksumAddress(exchange_group_keys), amount,
                                       Web3.toChecksumAddress(peer_wallets_contract_address)). \
                    buildTransaction({'from': self.operator,
                                      'gas': 200000,
                                      'chainId': 3,
                                      'nonce': int(self.w3.eth.getTransactionCount(AUTHORITY['address'])),
                                      'gasPrice': 41000000000})

                raw_tx = {'nonce': txn['nonce'],
                          'gas': txn['gas'],
                          'gasPrice': txn['gasPrice'],
                          'to': txn['to'],
                          'data': txn['data'],
                          'from': txn['from'],
                          'chainId': txn['chainId'],
                          }

                signed_txn = self.w3.eth.account.signTransaction(dict(raw_tx), AUTHORITY['key'])
                self.w3.eth.sendRawTransaction(signed_txn['rawTransaction'])
                tx_hash = Web3.toHex(Web3.sha3(signed_txn.rawTransaction))
                self.w3.eth.waitForTransactionReceipt(tx_hash)
                print("Transaction For Deposit Kyber Tokens Successfully Submitted, Hash : {0}".format(tx_hash))
