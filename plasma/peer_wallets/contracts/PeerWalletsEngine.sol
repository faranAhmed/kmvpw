pragma solidity 0.4.24;
import './PeerWallets.sol';


/**
 * @title PeerWalletsEngine
 * @dev To launch a peer wallet with more than one peers and specified distribution for each group
 */
contract PeerWalletsEngine {

    /**
     * Data Members
     */
    // Mapping of leader to newly created peerwallet
    // address -> leader
    // address -> peer wallet
    mapping (address => address) private leadWallet;

    /**
     * Public functions
     */
    /**
     * @dev Fnction to launch a new peer wallet after coniditons;
     * - If number of peers for a wallet are greater than 1
     * - If exchange groups are equal to the number of distributions
     * @param _leader address of leader
     * @param _peers array of addresses, peers for wallet
     * @param _exchangeGroups array of addresses, exchange groups (tokens) to invest
     * @param _distribution array of unsigned integer percentage distributions for exchange group
     * @param _amount is for the total value sent during peerwallet creation
     * @return address of newly created peerWallet successfully, address of account 0 otherwise
     * Note interacts from web-end
     */
    function launchPeerWallet(address _leader, address[] _peers, address[] _exchangeGroups, uint[] _distribution, uint _amount)
    external {
        if (_peers.length > 1 && _exchangeGroups.length == _distribution.length) {
            PeerWallets peerWalletsObj = new PeerWallets();
            leadWallet[_leader] = address(peerWalletsObj.createPeerWallet(_leader, _peers, _exchangeGroups, _distribution, _amount));
        }
    }

    /**
     * @dev Function to get peer wallet address and remove
     * @param _leader address of the leader
     * @return address of peerWallet
     * Note interacts from web-end
     */
    function getPeerWalletAddress(address _leader)
    external
    view
    returns (address) {
        return leadWallet[_leader];
    }
}
