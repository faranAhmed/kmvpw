pragma solidity 0.4.24;


/**
 * @title Relayer
 * @dev To make transaction from peerwallets to child-chain
 */
contract Relayer {

    /**
     * Events
     */
    // Event to raise for final investment
    event LogTotal(
        address indexed owner,
        uint indexed totalInvested,
        address indexed peerWallet
    );

    /**
     * External Functions
     */
    /**
     * @dev Function to trigger event for final investment
     * @param _amount to get total amount
     * @param _user address of the owner
     * Note interacts from web-end
     */
    function triggerEvent(uint _amount, address _user)
    external {
        emit LogTotal(_user, _amount, msg.sender);
    }
}