pragma solidity 0.4.24;
import './Relayer.sol';


/**
 * @title PeerWallets
 * @dev To create a wallet with more than one peers and buy desired tokens
 */
contract PeerWallets {

    /**
     * Contract Instances
     */
    // Relayer Smart Contract Object
    Relayer private relayerObj;
    
    /**
     * Storage
     */
    // total amount invested
    uint private totalInvested;

    // boolean if leader launched investment
    bool private investmentLaunched;

    // address of leading peer
    address private leader;

    // addresses of all peers
    address[] private peers;

    // addresses of all invested peers
    address[] private investedPeers;

    // addresses of desired exchange groups
    address[] private exchangeGroupKeys;

    // Percentage of distribution per exchange group
    uint[] private distribution;

    /**
     * Mappings
     */
    // mapping of peer ownership in wallet
    // address -> peer address
    // uint -> indicates the percentage out of 100
    mapping (address => uint) private ownership;

    // mapping of peers who invested in wallet
    // address -> peer address
    // uint8 -> indicates peer invested 1/0
    mapping (address => uint8) private isInvestedPeer;

    // mapping of amount invested by a peer
    // address -> peer address
    // uint -> indicates the amount of ethers
    mapping (address => uint) private peerAmount;

    // mapping of tokens owned by each peer
    // address -> peer address
    // string -> exchange Group key
    // uint -> indicates the amount of tokens
    mapping (address => mapping (address => uint)) private peerTokens;

    // mapping tokens for each member
    // string -> exchange Group id
    // uint -> amount of tokens earned by a wallet
    mapping (address => uint) private walletTokens;

    /**
     * Constructor
     */
    constructor()
    public {
        totalInvested = 0;
        investmentLaunched = false;
    }
    
    /**
     * View Functions
     */
    /**
     * @dev View all peers in wallet
     * @return address of all peers
     * Note interacts from web-end
     */
    function getPeers()
    external
    view
    returns (address[]) {
        return peers;
    }

    /**
     * @dev View all peers who invested in wallet
     * @return addresses of all invested peers
     * Note interacts from web-end
     */
    function getInvestedPeers()
    external
    view
    returns (address[]) {
        return investedPeers;
    }
    
    /**
     * @dev View total invested in wallet
     * @return amount total invested
     * Note interacts from web-end
     */
    function getTotalInvested()
    external
    view
    returns (uint) {
        return totalInvested;
    }

    /**
     * @dev View exchange Group Key at specified index
     * @param _index unsigned integer value
     * @return address of an exchange group
     * Note interacts from web-end
     */
    function getExchangeGroupsKeyAt(uint _index)
    external
    view
    returns (address) {
        return exchangeGroupKeys[_index];
    }

    /**
     * @dev View exchange Group Keys length
     * @return unsigned integer lenght
     * Note interacts from web-end
     */
    function getExchangeGroupsLength()
    external
    view
    returns (uint) {
        return exchangeGroupKeys.length;
    }

    /**
     * @dev View distribution at specified index
     * @param _index unsigned integer value
     * @return unsigned integer values of distribution
     * Note interacts from web-end
     */
    function getDistributionAt(uint _index)
    external
    view
    returns (uint) {
        return distribution[_index];
    }
     
    /**
     * @dev View if peer exists in wallet
     * @param _peer Address of a peer
     * @return True if exists in wallet, false otherwise
     * Note interacts from web-end
     */
    function validatePeer(address _peer)
    private
    view
    returns (bool) {
        for (uint i = 0; i < peers.length; ++i)
            if (peers[i] == _peer)
                return true;
        return false;
    }

    /**
     * @dev View ownership in peerwallet
     * @param _peer address of the peer
     * @return Percentage of ownership, 0 otherwise
     * Note interacts from web-end
     */
    function getPeerOwnership(address _peer)
    external
    view
    returns (uint) {
        if (validatePeer(_peer) == true)
            return ownership[_peer];
        return 0;
    }

    /**
     * @dev View tokens a peer owns
     * @param _peer address of the peer
     * @param _exchangeGroupKey address of the exchange group
     * @return array of unsigned integer, amount of tokens
     * Note interacts from web-end
     */
    function getPeerTokensAt(address _peer, address _exchangeGroupKey)
    external
    view
    returns (uint) {
        return peerTokens[_peer][_exchangeGroupKey];
    }

    /**
     * @dev View tokens wallet owns
     * @param _exchangeGroupKey address of the exchange Group
     * @return array of unsigned integer, amount of tokens
     * Note interacts from web-end
     */
    function getWalletTokens(address _exchangeGroupKey)
    external
    view
    returns (uint) {
        return walletTokens[_exchangeGroupKey];
    }

    /**
     * Functions
     */
    /**
     * @dev Function to create a wallet for peers with exchange groups and distribution of investment
     * @param _leader address of leading peer
     * @param _peers array of addresses peers
     * @param _exchangeGroupKeys array of addresses of exchange groups (tokens) to invest
     * @param _distribution array of unsigned integer a percentage for exchange group
     * @param _amount is the total investmetn sent for peer-wallet creation
     * @return Address of peer wallet
     * Note interacts from peerWalletEngine Smart Contract
     */
    function createPeerWallet(address _leader, address[] _peers, address[] _exchangeGroupKeys, uint[] _distribution, uint _amount)
    external
    returns (address) {
        leader = _leader;
        totalInvested = _amount;
        peerAmount[leader] = totalInvested;
        if (totalInvested > 0) {
            addInvestedPeer(leader);
        }
        peers = _peers;
        distribution = _distribution;
        exchangeGroupKeys = _exchangeGroupKeys;
        investmentLaunched = false;
        return this;
    }

    /**
     * @dev Function for leader to permit investments
     * @param _amount total amount of ethers sent
     * @param _peer address of the peer
     * Note interacts from web-end
     */
    function launchInvestment(uint _amount, address _peer)
    external {
        if (leader == _peer) {
            investmentLaunched = true;
            peerAmount[_peer] += _amount;
            totalInvested += _amount;
            addInvestedPeer(_peer);
        }
    }

    /**
     * @dev Function to make investment for wallet
     * @param _amount total amount of ethers sent
     * @return true if transaction successful, false otherwise
     * Note interacts from web-end
     */
    function makeInvestment(uint _amount, address _peer)
    external
    returns (bool) {
        if (validatePeer(_peer) == true) {
            peerAmount[_peer] += _amount;
            totalInvested += _amount;
            addInvestedPeer(_peer);
            if (investmentLaunched == true) {
                if (investedPeers.length == peers.length) {
                    triggerEvent(leader);
                    return true;
                }
            } else {
                triggerEvent(_peer);
                return true;
            }
        }
        return false;
    }

    /**
     * @dev Function for leader to trigger investment
     * @param _peer address of the peer
     * Note interacts from web-end
     */
    function triggerInvestment(address _peer)
    external {
        if (leader == _peer && totalInvested > 0)
            triggerEvent(leader);
    }

    /**
     * @dev Function to trade and distribute for all peers who invested
     * - trade ERC20 tokens
     * - distribute ownership in peerWallet for all peers
     * - distribute tokens for all peers
     * - reset peerWallet member values
     * @return true if distribution successful
     * Note interacts from web-end
     */
    function completeInvestment()
    external
    returns (bool) {
        distributeOwnership();
        distributePeerTokens();
        return true;
    }

    /**
     * @dev Function to set Kyber ERC20 tokens for exchange group key
     * @param _exchangeGroupKey address of the exchange group
     * @param _tokens amount of tokens for an exchange group
     * Note interacts from web-end
     */
    function tradeWalletTokens(address _exchangeGroupKey, uint _tokens)
    external {
        walletTokens[_exchangeGroupKey] = _tokens;
    }

    /**
     * @dev Function to distribute the ownership of each peer in wallet
     */
    function distributeOwnership()
    private {
        for (uint i = 0; i < investedPeers.length; ++i)
            ownership[investedPeers[i]] = (peerAmount[investedPeers[i]] * 100) / totalInvested;
    }

    /**
     * @dev Function to distribute tokens for each peer
     */
    function distributePeerTokens()
    private {
        for (uint j = 0; j < investedPeers.length; ++j)
            for (uint i = 0; i < exchangeGroupKeys.length; ++i)
                peerTokens[investedPeers[j]][exchangeGroupKeys[i]] =
                (walletTokens[exchangeGroupKeys[i]] * ownership[investedPeers[j]]) / 100;
    }

    /**
     * @dev Function to reset peer wallet data members
     */
    function resetPeerWallet()
    private {
        for (; investedPeers.length > 0;) {
            peerAmount[investedPeers[0]] = 0;
            isInvestedPeer[investedPeers[0]] = 0;
            investedPeers[0] = investedPeers[investedPeers.length - 1];
            delete investedPeers[investedPeers.length - 1];
            --investedPeers.length;
        }
        for (; exchangeGroupKeys.length > 0;) {
            walletTokens[exchangeGroupKeys[0]] = walletTokens[exchangeGroupKeys[exchangeGroupKeys.length - 1]];
            delete walletTokens[exchangeGroupKeys[exchangeGroupKeys.length - 1]];
            --exchangeGroupKeys.length;
        }
        totalInvested = 0;
    }

    /**
     * @dev Function to withdraw all tokens
     * @param _peer address of the peer
     * Note interacts from web-end
     */
    function withdrawAllTokens(address _peer)
    external {
        if (validatePeer(_peer) == true)
            for (uint i = 0; i < exchangeGroupKeys.length; ++i){
                if(ERC20(exchangeGroupKeys[i]).balanceOf(msg.sender) > 0)
                    ERC20(exchangeGroupKeys[i]).transfer(_peer, peerTokens[_peer][exchangeGroupKeys[i]]);
            }
    }

    /**
     * @dev Function to withdraw specified token
     * @param _exchangeGroupKey address of token
     * @param _peer address of the peer
     * Note interacts from web-end
     */
    function withdrawTokens(address _exchangeGroupKey, address _peer)
    external {
        if (validatePeer(_peer) == true)
            ERC20(_exchangeGroupKey).transfer(_peer, peerTokens[_peer][_exchangeGroupKey]);
    }

    /**
     * @dev Function to add a peer address to investedPeers
     * @param _peer address of peer
     */
    function addInvestedPeer(address _peer)
    private {
        if (isInvestedPeer[_peer] == 0) {
            investedPeers.push(_peer);
            isInvestedPeer[_peer] = 1;
        }
    }

    /**
     * @dev Function to add peers
     * @param _peers address of the peers
     * @param _leader address of the leader
     * @return true if peer added successfully, false otherwise
     * Note interacts from web-end
     */
    function addPeers(address[] _peers, address _leader)
    external
    returns (bool) {
        if (leader == _leader) {
            if (_peers.length > 1) {
                for (uint i = 0; i < _peers.length; ++i)
                    peers.push(_peers[i]);
            } else {
                peers.push(_peers[0]);
            }
            return true;
        }
        return false;
    }

    /**
     * @dev Function to remove a peer
     * @param _peer address of the peer
     * @param _leader address of the leader
     * Note interacts from web-end
     */
    function removePeer(address _peer, address _leader)
    external {
        if (leader == _leader) {
            if (peers[peers.length - 1] == _peer) {
                delete peers[peers.length - 1];
                peers.length--;
                return;
            } else {
                for (uint i = 0; i < peers.length; ++i)
                    if (peers[i] == _peer) {
                        peers[i] = peers[peers.length - 1];
                        delete peers[peers.length - 1];
                        peers.length--;
                        return;
                    }
            }
        }
    }

    /**
     * @dev Function to call Relayer Contract Trigger Event
     * @param _peer address of the investor
     */
    function triggerEvent(address _peer)
    private {
        relayerObj.triggerEvent(totalInvested, _peer);
    }
	
	/**
     * @dev Function to call Relayer Contract Trigger Event
     * @param _relayer address of the Relayer instance
	 * Note interacts from web-end
     */
    function triggerEvent(Relayer _relayer)
    public {
        relayerObj = Relayer(_relayer);
    }
}


/**
 * @title ERC20 interface
 * @dev Simpler version of ERC20 interface to just transfer tokens
 */
interface ERC20 {
    function transfer(address _to, uint _value) external returns (bool success);
    function balanceOf(address tokenOwner) external view returns (uint balance);
}
