from web3 import Web3
from plasma_core.constants import PEER_WALLETS_ADDRESS, AUTHORITY, HTTPProviderRopsten, root_chain_instance

w3 = HTTPProviderRopsten

# depositEther
txn = root_chain_instance.functions.depositEther(PEER_WALLETS_ADDRESS).\
    buildTransaction({'from': AUTHORITY['address'],
                      'gas': 900000,
                      'gasPrice': 41000000000,
                      'chainId': 3,
                      'value': 1,
                      'nonce': Web3.toHex(w3.eth.getTransactionCount(Web3.toChecksumAddress(AUTHORITY['address'])))})

raw_tx = {'nonce': txn['nonce'],
          'gas': txn['gas'],
          'gasPrice': txn['gasPrice'],
          'to': txn['to'],
          'data': txn['data'],
          'from': txn['from'],
          'chainId': txn['chainId'],
          'value': txn['value']
          }

signed_txn = w3.eth.account.signTransaction(dict(raw_tx), AUTHORITY['key'])
w3.eth.sendRawTransaction(signed_txn.rawTransaction)
tx_hash = Web3.toHex(Web3.sha3(signed_txn.rawTransaction))
receipt = w3.eth.waitForTransactionReceipt(tx_hash)
print("Transaction For Deposit Ether Successfully Submitted, Hash : {0}".format(tx_hash))
