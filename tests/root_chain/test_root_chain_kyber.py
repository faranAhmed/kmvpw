from web3 import Web3
from plasma_core.constants import PEER_WALLETS_ADDRESS, AUTHORITY, HTTPProviderRopsten, root_chain_instance

w3 = HTTPProviderRopsten

# depositKyberTokens
txn = root_chain_instance.functions.\
    depositKyberTokens(Web3.toChecksumAddress("0xaD6D458402F60fD3Bd25163575031ACDce07538D"), 1, Web3.toChecksumAddress(PEER_WALLETS_ADDRESS)).\
    buildTransaction({'from': AUTHORITY['address'],
                      'gas': 900000,
                      'gasPrice': 41000000000,
                      'chainId': 3,
                      'nonce': int(w3.eth.getTransactionCount(AUTHORITY['address']))})

raw_tx = {'nonce': txn['nonce'],
          'gas': txn['gas'],
          'gasPrice': txn['gasPrice'],
          'to': txn['to'],
          'data': txn['data'],
          'from': txn['from'],
          'chainId': txn['chainId']}

signed_txn = w3.eth.account.signTransaction(dict(raw_tx), AUTHORITY['key'])
w3.eth.sendRawTransaction(signed_txn['rawTransaction'])
tx_hash = Web3.toHex(Web3.sha3(signed_txn.rawTransaction))
w3.eth.waitForTransactionReceipt(tx_hash)
print("Transaction For Deposit Kyber Tokens Successfully Submitted, Hash : {0}".format(tx_hash))
