from web3 import Web3
from plasma_core.constants import PEER_WALLETS_ADDRESS, ACCOUNTS, HTTPProviderLocal, relayer_instance

w3 = HTTPProviderLocal

# triggerEvent
txn = relayer_instance.functions.triggerEvent(int(1), PEER_WALLETS_ADDRESS).\
    buildTransaction({'from': ACCOUNTS[0]['address'],
                      'gas': 900000,
                      'gasPrice': 41000000000,
                      'nonce': Web3.toHex(w3.eth.getTransactionCount(ACCOUNTS[0]['address']))})

raw_tx = {'nonce': txn['nonce'],
          'gas': txn['gas'],
          'gasPrice': txn['gasPrice'],
          'to': txn['to'],
          'data': txn['data'],
          'from': txn['from']}

signed_txn = w3.eth.account.signTransaction(dict(raw_tx), ACCOUNTS[0]['key'])
w3.eth.sendRawTransaction(signed_txn.rawTransaction)
tx_hash = Web3.toHex(Web3.sha3(signed_txn.rawTransaction))
w3.eth.waitForTransactionReceipt(tx_hash)
print("Transaction For Trigger Event Successfully Submitted, Hash : {0}".format(tx_hash))
