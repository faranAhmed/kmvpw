
from plasma_core.constants import peer_wallets_instance, AUTHORITY

print("Wallet Peers :")
print(peer_wallets_instance.functions.getPeers().call())
print()

print("Peers who invested :")
print(peer_wallets_instance.functions.getInvestedPeers().call())
print()

print("Total Wallet Amount in Wei :")
print(peer_wallets_instance.functions.totalInvested().call())
print()

print("Total Number of desired Exchange Groups :")
print(peer_wallets_instance.functions.getExchangeGroupsLength().call())
print()

print("Total OwnerShip of Peer :")
print(peer_wallets_instance.functions.getPeerOwnership(AUTHORITY['address']).call())
print()

print("Total Tokens of Peer (in Wei) : ")
totalToken = peer_wallets_instance.functions.getPeerTokensAt(AUTHORITY['address'], "0xaD6D458402F60fD3Bd25163575031ACDce07538D").call()
print(totalToken)
print()

print("Total Wallet Tokens :")
print(peer_wallets_instance.functions.getWalletTokens("0xaD6D458402F60fD3Bd25163575031ACDce07538D").call())
print()
