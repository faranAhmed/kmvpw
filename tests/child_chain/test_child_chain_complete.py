from web3 import Web3
from plasma_core.constants import HTTPProviderLocal, ACCOUNTS, peer_wallets_instance

# Testing Functions for Solidity Calls
w3 = HTTPProviderLocal

# completeInvestment
txn = peer_wallets_instance.functions.completeInvestment().\
    buildTransaction({'from': ACCOUNTS[0]['address'],
                      'gas': 900000,
                      'gasPrice': 41000000000,
                      'nonce': w3.eth.getTransactionCount(ACCOUNTS[0]['address'])})

raw_tx = {'nonce': txn['nonce'],
          'gas': txn['gas'],
          'gasPrice': txn['gasPrice'],
          'to': txn['to'],
          'data': txn['data'],
          'from': txn['from']}

signed_txn = w3.eth.account.signTransaction(dict(raw_tx), ACCOUNTS[0]['key'])
w3.eth.sendRawTransaction(signed_txn.rawTransaction)
tx_hash = Web3.toHex(Web3.sha3(signed_txn.rawTransaction))
w3.eth.waitForTransactionReceipt(tx_hash)
print("Transaction For Complete Investment Successfully Submitted, Hash : {0}".format(tx_hash))

print("Total Number of Tokens for User 1 :")
print(peer_wallets_instance.functions.getPeerTokensAt(ACCOUNTS[0]['address']).call())
print()
