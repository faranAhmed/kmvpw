from plasma_core.constants import HTTPProviderLocal, peer_wallets_engine_instance, AUTHORITY

# Testing Functions for Solidity Calls
w3 = HTTPProviderLocal

# triggerInvestment
txn = peer_wallets_engine_instance.functions.launchPeerWallet("0x8F5498bAEC61169FDd532073693BB4F789727514",
                                                              ["0xf76B6526f3B71C8ADf0e9CfA8E831c3E61F5Af44",
                                                               "0x8F5498bAEC61169FDd532073693BB4F789727514"],
                                                              ["0xaD6D458402F60fD3Bd25163575031ACDce07538D"],
                                                              [int(100)]).\
    buildTransaction({'from': AUTHORITY['address'],
                      'gas': 900000,
                      'gasPrice': 41000000000})

raw_tx = {'nonce': txn['nonce'],
          'gas': txn['gas'],
          'gasPrice': txn['gasPrice'],
          'to': txn['to'],
          'data': txn['data'],
          'from': txn['from']}

w3.eth.sendTransaction(dict(raw_tx))
print("Transaction For Launch Peer Wallet Successfully Submitted")
print()

address = peer_wallets_engine_instance.functions.getPeerWalletAddress("0xf76B6526f3B71C8ADf0e9CfA8E831c3E61F5Af44").call()
print("Transaction For Get Wallet Address Successfully Submitted, Address : {0}".format(address))
