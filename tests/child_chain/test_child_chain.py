from web3 import Web3

from plasma.peer_wallets.deployer import Deployer
from plasma_core.constants import ACCOUNTS, HTTPProviderLocal, peer_wallets_engine_instance, AUTHORITY

# Testing Functions for Solidity Calls
w3 = HTTPProviderLocal

# createPeerWallet
txn = peer_wallets_engine_instance.functions.launchPeerWallet(ACCOUNTS[0]['address'],
                                                              [ACCOUNTS[0]['address'], AUTHORITY['address']],
                                                              ["0xaD6D458402F60fD3Bd25163575031ACDce07538D"],
                                                              [int(100)]).\
    buildTransaction({'from': ACCOUNTS[0]['address'],
                      'gas': 900000,
                      'value': 0,
                      'gasPrice': 41000000000,
                      'nonce': w3.eth.getTransactionCount(ACCOUNTS[0]['address'])})

raw_tx = {'nonce': txn['nonce'],
          'gas': txn['gas'],
          'gasPrice': txn['gasPrice'],
          'to': txn['to'],
          'value': txn['value'],
          'data': txn['data'],
          'from': txn['from']}

signed_txn = w3.eth.account.signTransaction(dict(raw_tx), ACCOUNTS[0]['key'])
w3.eth.sendRawTransaction(signed_txn.rawTransaction)
tx_hash = Web3.toHex(Web3.sha3(signed_txn.rawTransaction))
w3.eth.waitForTransactionReceipt(tx_hash)
print("Transaction For Launch Peer Wallet Successfully Submitted, Hash : {0}".format(tx_hash))

peer_wallet_address = Web3.toChecksumAddress(peer_wallets_engine_instance.functions.getPeerWalletAddress(ACCOUNTS[0]).call())

print("Peer Wallet {0}".format(peer_wallet_address))

peer_wallets_instance = Deployer().get_contract_at_address_env('PeerWallets', peer_wallet_address, HTTPProviderLocal, False)

print("Wallet Peers :")
print(peer_wallets_instance.functions.getPeers().call())
print()

print("Peers who invested :")
print(peer_wallets_instance.functions.getInvestedPeers().call())
print()

print("Total Wallet Amount in Wei :")
print(peer_wallets_instance.functions.totalInvested().call())
print()

print("Total Number of desired Exchange Groups :")
print(peer_wallets_instance.functions.getExchangeGroupsLength().call())
print()

print("Total OwnerShip of Peer :")
print(peer_wallets_instance.functions.getPeerOwnership(AUTHORITY['address']).call())
print()

print("Total Tokens of Peer (in Wei) : ")
totalToken = peer_wallets_instance.functions.getPeerTokensAt(AUTHORITY['address'], "0xaD6D458402F60fD3Bd25163575031ACDce07538D").call()
print(totalToken)
print()

print("Total Wallet Tokens :")
print(peer_wallets_instance.functions.getWalletTokens("0xaD6D458402F60fD3Bd25163575031ACDce07538D").call())
print()
