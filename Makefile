init:
	python setup.py install

.PHONY: help
help:
	@echo "clean       - remove build artifacts"
	@echo "lint        - check style with flake8"
	@echo "test        - run tests with pytest"

.PHONY: peer-wallets
peer-wallets:
	python deployment.py

.PHONY: child-chain
child-chain:
	PYTHONPATH=. python plasma/child_chain/server.py

.PONY: test-child-chain - make peerwallet directly
test-child-chain:
	PYTHONPATH=. python tests/child_chain/test_child_chain.py

.PONY: test-trigger
test-trigger:
	PYTHONPATH=. python tests/child_chain/test_child_chain_trigger.py

.PONY: test-relayer
test-relayer:
	PYTHONPATH=. python tests/relayer/test_relayer.py

.PONY: test-root-chain
test-root-chain:
	PYTHONPATH=. python tests/root_chain/test_root_chain.py

.PONY: test-kyber
test-kyber:
	PYTHONPATH=. python tests/root_chain/test_root_chain_kyber.py

.PHONY: test-complete
test-complete:
	PYTHONPATH=. python tests/child_chain/test_child_chain_complete.py

.PHONY: test-force-complete
test-force-complete:
	PYTHONPATH=. python tests/child_chain/test_force_complete.py

.PHONY: test-calls
test-calls:
	PYTHONPATH=. python tests/child_chain/test_calls.py

.PHONY: launch-peer-wallet
launch-peer-wallet:
	PYTHONPATH=. python tests/child_chain/test_engine.py

.PHONY: clean
clean: clean-build clean-pyc

.PHONY: clean-build
clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info

.PHONY: clean-pyc
clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '*pycache__' -exec rm -rf {} +
	find . -name '.pytest_cache' -exec rm -rf {} +

.PHONY: dev
dev:
	pip install pytest pylint flake8

.PHONY: ganache
ganache:
	ganache-cli --account "0x70f1384b24df3d2cdaca7974552ec28f055812ca5e4da7a0ccd0ac0f8a4a9b00,30000000000000000000000000000000000"
